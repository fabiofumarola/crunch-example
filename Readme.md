This is the word count example for Apache Crunch Library. The library is not well documented and there is only one example.
In this tutorial I used the following tutorials that should be read and increased with others

1. http://www.hadoopsphere.com/2013/03/apache-crunch-laying-pipelines-for.html
2. http://www.infoq.com/articles/ApacheCrunch
3. http://www.drdobbs.com/database/introduction-to-hadoop-real-world-hadoop/240153375
4. http://puffsun.iteye.com/blog/1915201


Crunch is based on the concept of pipeline, which represents a Directed acyclic graph. 

For instance, to implement the common Word Count example, an Apache Crunch program would have the following steps:

Create a Pipeline object
Read input (e.g. text file) into a PCollection
Execute various functions on input data 
e.g. 
PTable<String, Long> counts =
  lines.parallelDo(extractWord,
    Writables.strings())
        .count();
Or
PCollection<String> words = lines.parallelDo(new DoFn<String, String>() {
public void process(String line, Emitter<String> emitter) {
        for (String word : line.split("\\s+")) {
          emitter.emit(word);
        }
      }
    }, Writables.strings());

 PTable<String, Long> counts = words.count();

Persist the output collection and execute the pipeline.

Unlike Cascading, Crunch does not impose a single data type that all of its inputs must conform to. Instead, Crunch uses a customizable type system that is flexible enough to work directly with complex data such as time series, HDF5 files, Apache HBase tables, and serialized objects like protocol buffers or Avro records.

This is done using the org.apache.crunch.io.From Class which offers the mothod to read from different sources.
http://crunch.apache.org/apidocs/0.7.0/org/apache/crunch/io/From.html

Pipeline pipeline = new MRPipeline(this.getClass()); 
// Reference the lines of a text file by wrapping the TextInputFormat class. 
PCollection lines = pipeline.read(From.textFile("/path/to/myfiles")); 
// Reference entries from a sequence file where the key is a LongWritable and the 
// value is a custom Writable class. 
PTable table = pipeline.read(From.sequenceFile( "/path/to/seqfiles", LongWritable.class, MyWritable.class)); 
// Reference the records from an Avro file, where MyAvroObject implements Avro's SpecificRecord interface. 
PCollection myObjects = pipeline.read(From.avroFile("/path/to/avrofiles", MyAvroObject.class)); 
// References the key-value pairs from a custom extension of FileInputFormat: 
PTable custom = pipeline.read(From.formattedFile( "/custom", MyFileInputFormat.class, KeyWritable.class, ValueWritable.class));

Crunch Concepts

Crunch's core abstractions are a PCollection<T>, which represents a distributed, immutable collection of objects, and a PTable<K, V>, which is a sub-interface of PCollection that contains additional methods for working with key-value pairs. These two core classes support four primitive operations:

1. parallelDo: Apply a user-defined function to a given PCollection and return a new PCollection as a result.
2. groupByKey: Sort and group the elements of a PTable by their keys (equivalent to the shuffle phase of a MapReduce job).
3. combineValues: Perform an associative operation to aggregate the values from a groupByKey operation.
4. union: Treat two or more PCollections as a single, virtual PCollection.

The classes Sort, Aggregate, Sample and so on offer the functions to apply function to collections and tables. 
For example the sort function enables to sort collection and tables based on the key, following an order, or by specifying the columns.

To sort by column 2 ascending then column 1 descending, you would use: sortPairs(coll, by(2, ASCENDING), by(1, DESCENDING)) Column numbering is 1-based.

All the functionalities are explained at the javadoc http://crunch.apache.org/apidocs/0.7.0/



